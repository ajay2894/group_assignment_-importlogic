/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Abstract.User;
import Business.Users.Supplier;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ajay Mohandas, Gurjot Kaur, Harsh Shah
 */
public class SupplierDirectory {
    
    private List<User> supplierList;
    
    public SupplierDirectory(){
        supplierList = new ArrayList<>();
    }

    public List<User> getSupplierList() {
        return supplierList;
    }

    public void setSupplierList(List<User> supplierList) {
        this.supplierList = supplierList;
    }
    //Below is the code for adding suppliers in the array list
    public Supplier addSupplier(String password,String username){
        
        Supplier supplier = new Supplier(password,username);
        supplier.setUserName(username);
        supplier.setPassword(password);
        supplierList.add(supplier);
        return supplier;
    
    }
    
}

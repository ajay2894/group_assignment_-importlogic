/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Abstract.User;
import Business.Users.Customer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Ajay Mohandas, Gurjot Kaur, Harsh Shah
 */
public class CustomerDirectory {
    
    private List<User> customerList;
    
    public CustomerDirectory(){
        customerList = new ArrayList<>();
    }

    public List<User> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<User> customerList) {
        this.customerList = customerList;
    }
    //Below is the code for adding customers in the array list
    public Customer addCustomer(String password,String username){
        
        Customer customer = new Customer(password,username);
        customer.setUserName(username);
        customer.setPassword(password);
        customer.setDate(new Date());
        customerList.add(customer);
        return customer;
    
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import Business.CustomerDirectory;
import java.util.Date;

/**
 *
 * @author Ajay Mohandas, Gurjot Kaur, Harsh Shah
 */
public class Customer extends User implements Comparable <Customer> {       //Cusomer extends User.java and implement the interface of Comparable
    
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    public Customer(String password, String userName) {
        super(password, userName, "CUSTOMER");             
       
    }
   
    
     public int compareTo(Customer o) {
        return o.getUserName().compareTo(this.getUserName());
    }
     
   
     @Override
    public String toString() {
        return getUserName();           //To get username of customer
    }

    @Override
    public boolean verify(String password) {
     if(password.equals(getPassword()))
            return true;
        return false;
    }

  

   
}
